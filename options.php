<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
defined('ADMIN_MODULE_NAME') or define('ADMIN_MODULE_NAME', 'pvdy.feedback');

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Pvdy\Feedback\Config;


$app = Application::getInstance();
$context = $app->getContext();
$request = $context->getRequest();

$module_id = "pvdy.feedback";
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/options.php");

$RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($RIGHT >= "R") :
    $arAllOptions = [

        array(Config::OPTION_REGISTER, Loc::getMessage('PVDY_FEEDBACK_OPTION_REGISTER'), array("checkbox")),
    ];

    $aTabs = array(
        array(
            "DIV" => "main_settings",
            "TAB" => GetMessage("MAIN_TAB_SET"),
            "ICON" => "perfmon_settings",
            "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")
        )

    );

    $tabControl = new CAdminTabControl("tabControl", $aTabs);

    CModule::IncludeModule($module_id);

    if ($REQUEST_METHOD == "POST" && strlen($Update . $Apply . $RestoreDefaults) > 0 && $RIGHT == "W" && check_bitrix_sessid()) {
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/perfmon/prolog.php");

        if (strlen($RestoreDefaults) > 0)
            COption::RemoveOption("WE_ARE_CLOSED_TEXT");
        else {
            foreach ($arAllOptions as $arOption) {
                $sName = $arOption[0];
                $sVal = trim($_REQUEST[$sName], " \t\n\r");
                $sType = $arOption[2][0];

                if ($sType === 'heading') {
                    continue;
                }

                if ($sType === 'checkbox' && $sVal != 'Y') {
                    $val = 'N';
                }

                COption::SetOptionString($module_id, $sName, $sVal, $arOption[1]);
            }
        }

        ob_start();
        $Update = $Update . $Apply;
        require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php");
        ob_end_clean();
    }

    ?>

    <? if (count($arErrors) > 0): ?>
    <div id="tools_result_div">
        <? foreach ($arErrors as $error): ?>
            <div class="adm-info-message-wrap adm-info-message-red">
                <div class="adm-info-message">
                    <div class="adm-info-message-title"><?= $error['NAME'] ?></div>
                    <?= $error['TEXT'] ?>
                    <div class="adm-info-message-icon"></div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>

    <form method="post"
          action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= urlencode($module_id) ?>&amp;lang=<?= LANGUAGE_ID ?>">
        <?
        $tabControl->Begin();
        $tabControl->BeginNextTab();

        foreach ($arAllOptions as $arOption):
            $arType = $arOption[2];
            $note = $arOption[3] ?: null; ?>
            <? if ($arType[0] == "heading"): ?>
            <tr class="heading">
                <td colspan="2">
                    <b><? echo $arOption[1] ?></b>
                </td>
            </tr>
        <? else: ?>
            <? $val = COption::GetOptionString($module_id, $arOption[0]); ?>
            <tr>
                <td width="40%">
                    <label for="<?= htmlspecialcharsbx($arOption[0]) ?>"><?= $arOption[1] ?>
                        <? if ($note !== null): ?>
                            <span class="required"><sup><?= $note ?></sup></span>
                        <? endif; ?>
                        :
                    </label>
                </td>
                <td width="60%">
                    <? if ($arType[0] == "checkbox"): ?>
                        <input type="checkbox" name="<? echo htmlspecialcharsbx($arOption[0]) ?>"
                               id="<? echo htmlspecialcharsbx($arOption[0]) ?>"
                               value="Y"<? if ($val == "Y") echo " checked"; ?>>
                    <? elseif ($arType[0] == "text"): ?>
                        <input type="text" size="<? echo $arType[1] ?>" maxlength="255"
                               value="<? echo htmlspecialcharsbx($val) ?>"
                               name="<? echo htmlspecialcharsbx($arOption[0]) ?>"
                               id="<? echo htmlspecialcharsbx($arOption[0]) ?>">
                    <? elseif ($arType[0] == "password"): ?>
                        <input type="password" autocomplete="off" size="<? echo $arType[1] ?>" maxlength="255"
                               value="<? echo htmlspecialcharsbx($val) ?>"
                               name="<? echo htmlspecialcharsbx($arOption[0]) ?>"
                               id="<? echo htmlspecialcharsbx($arOption[0]) ?>">
                    <? elseif ($arType[0] == "textarea"): ?>
                        <textarea rows="<? echo $arType[1] ?>" cols="<? echo $arType[2] ?>"
                                  name="<? echo htmlspecialcharsbx($arOption[0]) ?>"
                                  id="<? echo htmlspecialcharsbx($arOption[0]) ?>"><? echo htmlspecialcharsbx($val) ?></textarea>
                    <? elseif ($arType[0] == "selectbox"):
                        echo SelectBoxFromArray($arOption[0], $arType[1], $val);
                        ?>
                    <?
                    endif ?>
                </td>
            </tr>
        <? endif; ?>
        <? endforeach ?>
        <? $tabControl->Buttons(); ?>
        <input type="submit" name="Update" value="<?= GetMessage("MAIN_SAVE") ?>"
               title="<?= GetMessage("MAIN_OPT_SAVE_TITLE") ?>">
        <input type="submit" name="Apply" value="<?= GetMessage("MAIN_OPT_APPLY") ?>"
               title="<?= GetMessage("MAIN_OPT_APPLY_TITLE") ?>">
        <? if ($_REQUEST["back_url_settings"] != ""): ?>
            <input type="button" name="Cancel" value="<?= GetMessage("MAIN_OPT_CANCEL") ?>"
                   title="<?= GetMessage("MAIN_OPT_CANCEL_TITLE") ?>"
                   onclick="window.location='<? echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"])) ?>'">
            <input type="hidden" name="back_url_settings"
                   value="<?= htmlspecialcharsbx($_REQUEST["back_url_settings"]) ?>">
        <? endif ?>

        <?= bitrix_sessid_post(); ?>
        <? $tabControl->End(); ?>
    </form>
    <?
    if (!empty($arNotes)) {
        echo BeginNote();
        foreach ($arNotes as $i => $str) {
            ?>
            <span class="required">
            <sup><?
                echo $i + 1 ?></sup></span><?
            echo $str ?>
            <br><?
        }
        echo EndNote();
    }
    ?>
<? endif; ?>