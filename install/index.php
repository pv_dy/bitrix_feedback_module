<?

use Bitrix\Highloadblock\HighloadBlockLangTable;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\UserFieldTable;
use Pvdy\Feedback\Config;
use Pvdy\Feedback\Event\FeedbackHandlers;
use Pvdy\Feedback\Helpers\AccessHelper;

IncludeModuleLangFile(__FILE__);

class pvdy_feedback extends CModule
{
    public const VENDOR_NAME = 'pvdy';
    public $MODULE_ID = 'pvdy.feedback';
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_CSS;
    public $strError = '';
    protected $installDir = 'bitrix';
    protected $config = [];

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = GetMessage('PVDY_FEEDBACK_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('PVDY_FEEDBACK_MODULE_DESC');

        $this->PARTNER_NAME = GetMessage("PVDY_FEEDBACK_PARTNER_NAME");
        $this->PARTNER_URI = GetMessage("PVDY_FEEDBACK_PARTNER_URI");

        if (strpos(str_replace('\\', '/', __FILE__), '/local/modules/' . $this->MODULE_ID . '/install/index.php')) {
            $this->installDir = 'local';
        }
        if (!class_exists(Config::class)) {
            require(realpath(__DIR__ . '/../lib/config.php'));
        }
        if (!class_exists(AccessHelper::class)) {
            require(realpath(__DIR__ . '/../lib/helpers/accesshelper.php'));
        }
    }

    function doInstall()
    {
        $this->checkCompatible();
        $this->installFiles();
        $this->installEvents();
        $this->installDB();
        global $APPLICATION;
        if ($APPLICATION->GetException()) {
            return false;
        }
        ModuleManager::registerModule($this->MODULE_ID);
    }

    function checkCompatible()
    {
        global $APPLICATION;

        try {
            AccessHelper::checkMainVersion();
            AccessHelper::checkModules();
        } catch (\Exception $e) {
            $APPLICATION->ThrowException($e->getMessage());
        }
    }

    function installFiles($arParams = array())
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . '/' . $this->installDir . '/modules/' . $this->MODULE_ID . '/install/components', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/' . self::VENDOR_NAME, true, true);
    }

    function installEvents()
    {
        EventManager::getInstance()->registerEventHandler('', Config::HL_NAME . 'OnAfterAdd', $this->MODULE_ID, FeedbackHandlers::class, 'onAfterFeedbackAdd');
    }

    function installDB($arParams = array())
    {
        global $APPLICATION;
        Option::set($this->MODULE_ID, Config::OPTION_REGISTER, 'Y');
        $connection = Application::getConnection();
        $connection->startTransaction();
        $addResult = HighloadBlockTable::add([
            'NAME' => Config::HL_NAME,
            'TABLE_NAME' => Config::HL_TABLE_NAME,
        ]);
        if (!$addResult->isSuccess()) {
            $APPLICATION->ThrowException($addResult->getErrorMessages()[0]);
            $connection->rollbackTransaction();
            return;
        }
        $hlId = $addResult->getId();
        $addResult = HighloadBlockLangTable::add([
            'ID' => $hlId,
            'LID' => 'ru',
            'NAME' => Loc::getMessage('PVDY_FEEDBACK_HL_NAME', null, 'ru'),
        ]);
        if (!$addResult->isSuccess()) {
            $APPLICATION->ThrowException($addResult->getErrorMessages()[0]);
            $connection->rollbackTransaction();
            return;
        }
        $hlFields = Config::getFields();
        foreach ($hlFields as $field) {
            $field['ENTITY_ID'] = 'HLBLOCK_' . $hlId;
            $userFieldObj = new \CUserTypeEntity;
            $id = $userFieldObj->Add($field);
            if (!$id) {
                // already throws exception
                $connection->rollbackTransaction();
                return;
            }
        }
        $connection->commitTransaction();
    }

    function doUninstall()
    {
        $this->checkCompatible();
        ModuleManager::unRegisterModule($this->MODULE_ID);
        $this->unInstallDB();
        $this->unInstallEvents();
        $this->unInstallFiles();
        global $APPLICATION;
        if ($APPLICATION->GetException()) {
            return false;
        }
    }

    function unInstallDB($arParams = array())
    {
        Option::delete($this->MODULE_ID);
        Application::getInstance()->getTaggedCache()->clearByTag(Config::HL_NAME);
        $connection = Application::getConnection();
        $connection->startTransaction();
        $foundHl = HighloadBlockTable::getList([
            'select' => [
                'ID'
            ],
            'filter' => [
                'NAME' => Config::HL_NAME
            ],
            'limit' => 1,
        ])->fetch();
        if (!$foundHl) {
            $connection->rollbackTransaction();
            return;
        }
        HighloadBlockLangTable::delete($foundHl['ID']);
        $foundHlFields = UserFieldTable::getList([
            'select' => [
                'ID'
            ],
            'filter' => [
                'ENTITY_ID' => 'HLBLOCK_' . $foundHl['ID']
            ],
        ])->fetchAll();
        foreach ($foundHlFields as $foundHlField) {
            $userFieldObj = new CUserTypeEntity;
            $userFieldObj->Delete($foundHlField['ID']);
        }
        HighloadBlockTable::delete($foundHl['ID']);
        $connection->commitTransaction();
    }

    function unInstallEvents()
    {
        EventManager::getInstance()->unRegisterEventHandler('', Config::HL_NAME . 'OnAfterAdd', $this->MODULE_ID, FeedbackHandlers::class, 'onAfterFeedbackAdd');
    }

    function unInstallFiles()
    {
        $moduleComponentsDir = $_SERVER['DOCUMENT_ROOT'] . '/' . $this->installDir . '/modules/' . $this->MODULE_ID . '/install/components';
        $installedComponentsPath = 'bitrix/components/' . self::VENDOR_NAME;
        $installedComponentsFullPath = $_SERVER['DOCUMENT_ROOT'] . '/' . $installedComponentsPath;
        if (is_dir($moduleComponentsDir)) {
            $d = dir($moduleComponentsDir);
            while ($entry = $d->read()) {
                if ($entry == "." || $entry == "..")
                    continue;
                DeleteDirFilesEx($installedComponentsPath . '/' . $entry);
            }
            $d->close();
            if (count((new Bitrix\Main\IO\Directory($installedComponentsFullPath))->getChildren()) === 0) {
                @rmdir($installedComponentsFullPath);
            }
        }
    }
}
