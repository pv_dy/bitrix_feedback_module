<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arCurrentValues */
Loader::includeModule('iblock');

$arComponentParameters = [
    'GROUPS' => [],
    'PARAMETERS' => [
        'ITEMS_COUNT' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('PVDY_FEEDBACK_LIST_PARAM_ITEMS_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '5',
        ],
        'CACHE_TIME' => ['DEFAULT' => 36000000],
    ],
];

$arComponentParameters['PARAMETERS']['PAGER_TEMPLATE'] = Array(
    'PARENT' => 'PAGER_SETTINGS',
    'NAME' => Loc::getMessage('PVDY_FEEDBACK_LIST_PARAM_PAGER_TEMPLATE'),
    'TYPE' => 'STRING',
    'DEFAULT' => '',
);