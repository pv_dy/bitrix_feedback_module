<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Type\Date;
use Bitrix\Main\UI\PageNavigation;
use Pvdy\Feedback\Config;
use Pvdy\Feedback\Helpers\AccessHelper;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

class PvdyFeedbackListComponent extends CBitrixComponent
{
    /**
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $arParams['ITEMS_COUNT'] = (int)$arParams['ITEMS_COUNT'] > 0 ? (int)$arParams['ITEMS_COUNT'] : 5;
        $arParams['CACHE_TIME'] = (int)$arParams['CACHE_TIME'] > 0 ? (int)$arParams['CACHE_TIME'] : 3600;
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * @return mixed|void|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        try {
            $this->checkModules();
        } catch (\Exception $e) {
            $this->__showError($e->getMessage());
            return;
        }
        $navObject = new PageNavigation('feedback-list');
        $navObject->setPageSize($this->arParams['ITEMS_COUNT'])
            ->initFromUri();
        if ($this->startResultCache(false, md5($navObject->getCurrentPage() . '|' . $navObject->getPageSize()))) {
            Application::getInstance()->getTaggedCache()->registerTag(Config::HL_NAME);
            $this->prepareResult($navObject);
            $this->endResultCache();
        }
        $navObject->setRecordCount($this->arResult['TOTAL_COUNT']);
        ob_start();
        global $APPLICATION;
        $APPLICATION->IncludeComponent(
            'bitrix:main.pagenavigation',
            '',
            array(
                'NAV_OBJECT' => $navObject,
                'SEF_MODE' => 'N',
            ),
            false
        );
        $this->arResult['NAV_STRING'] = ob_get_clean();
        $this->includeComponentTemplate();
    }

    /**
     * @return void
     * @throws LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('pvdy.feedback')) {
            throw new LoaderException('PVDY_FEEDBACK_ADD_NOT_INSTALLED_MODULE');
        }
        AccessHelper::checkModules();
    }

    /**
     * @param $navObject
     * @return void
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    protected function prepareResult($navObject)
    {
        $entity = HighloadBlockTable::compileEntity(Config::HL_NAME);
        $queryResult = $entity->getDataClass()::query()
            ->setSelect(['*'])
            ->setOrder([
                'UF_DATETIME' => 'DESC',
                'UF_FIO' => 'ASC'
            ])
            ->setOffset($navObject->getOffset())
            ->setLimit($navObject->getLimit())
            ->countTotal(true)
            ->exec();
        $this->arResult['TOTAL_COUNT'] = $queryResult->getCount();
        $this->arResult['ITEMS'] = $queryResult->fetchAll();
        foreach ($this->arResult['ITEMS'] as &$item) {
            foreach ($item as &$field) {
                if ($field instanceof \Bitrix\Main\Type\DateTime || $field instanceof Date) {
                    $field = $field->toString();
                }
            }
        }
        unset($item, $field);
    }
}