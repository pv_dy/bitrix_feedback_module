<?php
/**
 * @global $APPLICATION
 * @var array $arResult
 */

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

Extension::load("ui.bootstrap4");
if (!empty($arResult['ITEMS'])) {
    ?>
    <div class="feedback-list-outer">
        <div class="feedback-list mb-5">
            <?php
            foreach ($arResult['ITEMS'] as $item) {
                ?>
                <div class="feedback-card card mb-3 p-3">
                    <div class="feedback-card__name">
                        <?= $item['UF_FIO'] ?>
                    </div>
                    <div class="feedback-card__email text-muted">
                        <?= $item['UF_EMAIL'] ?>
                    </div>
                    <div class="feedback-card__phone text-muted">
                        <?= $item['UF_PHONE'] ?>
                    </div>
                    <div class="feedback-card__question">
                        <?= $item['UF_QUESTION'] ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
        echo $arResult['NAV_STRING'];
        ?>
    </div>
    <?php
}
else {
    echo '<div class="alert alert-danger">' . Loc::getMessage('PVDY_FEEDBACK_LIST_ITEMS_NOT_FOUND') . '</div>';
}
