<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    'NAME' => GetMessage('PVDY_FEEDBACK_COMPONENT_NAME'),
    'DESCRIPTION' => GetMessage('PVDY_FEEDBACK_COMPONENT_DESCRIPTION'),
    'COMPLEX' => 'Y',
    'PATH' => array(
        'ID' => 'pvdy',
        'CHILD' => array(
            'ID' => 'pvdy_feedback',
            'NAME' => GetMessage('PVDY_FEEDBACK'),
        ),
    ),
);
