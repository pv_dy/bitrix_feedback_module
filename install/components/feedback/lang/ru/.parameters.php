<?php

$MESS['PVDY_FEEDBACK_PARAM_ITEMS_COUNT'] = 'Количество элементов на странице';
$MESS['PVDY_FEEDBACK_PARAM_SEF_FOLDER'] = 'Количество элементов на странице';
$MESS['PVDY_FEEDBACK_PARAM_PAGER_TEMPLATE'] = 'Шаблон постраничной навигации';
$MESS['PVDY_FEEDBACK_PARAM_FORM_TITLE'] = 'Заголовок формы добавления';
$MESS['PVDY_FEEDBACK_PARAM_SHOW_FORM'] = 'Показывать форму добавления';