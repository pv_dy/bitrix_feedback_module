<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/** @var array $arCurrentValues */
Loader::includeModule('iblock');

$arComponentParameters = [
    'GROUPS' => [],
    'PARAMETERS' => [
        'ITEMS_COUNT' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('PVDY_FEEDBACK_PARAM_ITEMS_COUNT'),
            'TYPE' => 'STRING',
            'DEFAULT' => '5',
        ],
        'SHOW_FORM' => [
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('PVDY_FEEDBACK_PARAM_SHOW_FORM'),
            'TYPE' => 'CHECKBOX',
            'DEFAULT' => 'Y',
        ],
        'CACHE_TIME' => ['DEFAULT' => 36000000],
    ],
];
if ($arCurrentValues['SHOW_FORM'] !== 'N') {
    $arComponentParameters['PARAMETERS']['FORM_TITLE'] = [
        'PARENT' => 'BASE',
        'NAME' => Loc::getMessage('PVDY_FEEDBACK_PARAM_FORM_TITLE'),
        'TYPE' => 'STRING',
        'DEFAULT' => '',
    ];
}
$arComponentParameters['PARAMETERS']['PAGER_TEMPLATE'] = array(
    'PARENT' => 'PAGER_SETTINGS',
    'NAME' => Loc::getMessage('PVDY_FEEDBACK_PARAM_PAGER_TEMPLATE'),
    'TYPE' => 'STRING',
    'DEFAULT' => '',
);