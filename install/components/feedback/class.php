<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

class PvdyFeedbackComponent extends CBitrixComponent
{
    /**
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $arParams['ITEMS_COUNT'] = (int)$arParams['ITEMS_COUNT'] > 0 ? (int)$arParams['ITEMS_COUNT'] : 5;
        $arParams['CACHE_TIME'] = (int)$arParams['CACHE_TIME'] > 0 ? (int)$arParams['CACHE_TIME'] : 3600;
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * @return mixed|void|null
     */
    public function executeComponent()
    {
        $this->includeComponentTemplate('list');
    }
}