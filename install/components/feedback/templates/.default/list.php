<?php
/**
 * @global $APPLICATION
 * @var array $arParams
 * @var CBitrixComponent $component
 */
?>
<div class="container">
    <div class="mb-5 mt-5 feedback-wrapper">
        <div class="row">
            <?php
            if ($arParams['SHOW_FORM'] === 'Y') {
                ?>
                <div class="col-md-4 order-md-1">
                    <?php
                    $APPLICATION->IncludeComponent('pvdy:feedback.add', '', [
                        'FORM_TITLE' => $arParams['FORM_TITLE'],
                    ], $component);
                    ?>
                </div>
                <?php
            }
            ?>
            <div class="<?= $arParams['SHOW_FORM'] === 'Y' ? 'col-md-8' : 'col-12' ?>">
                <?php
                $APPLICATION->IncludeComponent('pvdy:feedback.list', '', [
                    'CACHE_TIME' => $arParams['CACHE_TIME'],
                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                    'ITEMS_COUNT' => $arParams['ITEMS_COUNT'],
                ], $component);
                ?>
            </div>
        </div>
    </div>
</div>

