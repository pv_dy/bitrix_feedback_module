<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;
use Bitrix\Main\UserField\{Renderer, Types\BaseType};
use Bitrix\Main\Web\Json;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
Extension::load("ui.bootstrap4");
/**
 * @var array $arResult
 */
$jsParams = [
    'CONTAINER_CLASS' => 'js-feedback-add-form-wpapper',
    'FORM_CLASS' => 'js-feedback-add-form',
]
?>
<div class="hlform-wrapper mb-5 <?= $jsParams['CONTAINER_CLASS'] ?>">
    <form class="hlform <?= $jsParams['FORM_CLASS'] ?>" novalidate>
        <?php
        if (!empty($arParams['FORM_TITLE'])) {
            ?>
            <h4><?= $arParams['FORM_TITLE'] ?></h4>
            <?php
        }
        ?>
        <div class="hlform__body">
            <?php
            foreach ($arResult['FIELDS'] as $field) {
                ?>
                <div class="row mt-3">
                    <div class="col-12">
                        <?php
                        echo '<label>' . $field['EDIT_FORM_LABEL'] . '</label>';
                        ?>
                    </div>
                    <div class="col-12">
                        <?php
                        echo (new Renderer($field, [
                            'mode' => BaseType::MODE_EDIT,
                            'attribute' => $field['ATTRS']
                        ]))->render();
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <div class="hlform__actions">
            <button type="submit"
                    class="btn btn-primary mt-3"><?= Loc::getMessage('PVDY_FEEDBACK_ADD_TPL_SUBMIT') ?></button>
        </div>
    </form>
</div>

<script>
    new PvdyFeedbackAddComponent(<?=Json::encode($jsParams)?>);
</script>