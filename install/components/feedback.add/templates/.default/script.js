(function () {
    window.PvdyFeedbackAddComponent = function (params) {
        this.params = params;
        this.container = null;
        this.form = null;
        BX.ready(BX.delegate(this.init, this));
    }
    window.PvdyFeedbackAddComponent.prototype = {
        init: function () {
            this.container = BX.findChild(document,
                {'class': this.params['CONTAINER_CLASS']}, true);
            this.form = BX.findChild(document,
                {'class': this.params['FORM_CLASS']}, true);
            if (!this.container || !this.form) {
                return;
            }
            BX.bind(this.form, 'submit',
                BX.delegate(this.processForm, this));
        },
        processForm: function (e) {
            e.preventDefault();
            let _this = this,
                formData = BX.ajax.prepareForm(e.target).data;
            e.target.classList.add('was-validated');
            if (e.target.checkValidity() === false) {
                return;
            }
            BX.ajax.runComponentAction(
                'pvdy:feedback.add',
                'add',
                {
                    'mode': 'class',
                    'method': 'post',
                    'data': {
                        'fields': formData,
                        'sessid': BX.sessid
                    },
                }).then(function (response) {
                BX.adjust(_this.container, {html: response.data.html});
            }, function (response) {
                let errorMessage = '';
                response.errors.forEach(function (item) {
                    errorMessage += item.message;
                })
                BX.addClass(_this.container, '_error');
                BX.adjust(_this.container, {html: errorMessage});
            });
            return false;
        }
    }
})()