<?php
/**
 * @var array $arResult
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

foreach ($arResult['FIELDS'] as $key => &$field) {
    if ($field['EDIT_IN_LIST'] !== 'Y') {
        unset($arResult['FIELDS'][$key]);
        continue;
    }
    $field['ATTRS'] = [];
    if ($field['MANDATORY'] === 'Y') {
        $field['ATTRS']['required'] = 'required';
    }
    $field['ATTRS']['class'] = 'form-control';
    if(!empty($field['SETTINGS']['REGEXP'])) {
        $field['ATTRS']['pattern'] = trim($field['SETTINGS']['REGEXP'], ' \n\r\t\v\x00/');
    }
}
unset($field);