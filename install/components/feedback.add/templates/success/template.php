<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

Extension::load("ui.bootstrap4");

echo '<span class="alert alert-success">' . Loc::getMessage('SUCCESS_TEXT') . '</span>';