<?php

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\Engine\ActionFilter\Csrf;
use Bitrix\Main\Engine\ActionFilter\HttpMethod;
use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\Response\Component;
use Bitrix\Main\Errorable;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\UserField\Internal\UserFieldHelper;
use Pvdy\Feedback\Config;
use Pvdy\Feedback\Helpers\AccessHelper;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();

class PvdyFeedbackAddComponent extends CBitrixComponent implements Controllerable, Errorable
{
    protected $errorCollection;

    /**
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams): array
    {
        $this->errorCollection = new ErrorCollection();
        return parent::onPrepareComponentParams($arParams);
    }

    /**
     * @return mixed|void|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function executeComponent()
    {
        try {
            $this->checkModules();
        } catch (\Exception $e) {
            $this->__showError($e->getMessage());
            return;
        }
        $hlId = HighloadBlockTable::query()
            ->setSelect(['ID'])
            ->where('NAME', Config::HL_NAME)
            ->exec()
            ->fetch();
        $this->arResult['FIELDS'] = UserFieldHelper::getInstance()->getManager()->GetUserFields('HLBLOCK_' . $hlId['ID'], 0, LANGUAGE_ID);
        $this->includeComponentTemplate();
    }

    /**
     * @return void
     * @throws LoaderException
     * @throws \Bitrix\Main\SystemException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('pvdy.feedback')) {
            throw new LoaderException('PVDY_FEEDBACK_ADD_NOT_INSTALLED_MODULE');
        }
        AccessHelper::checkModules();
    }

    /**
     * @param $fields
     * @return Component|null
     * @throws \Bitrix\Main\SystemException
     */
    public function addAction($fields)
    {
        try {
            $this->checkModules();
        } catch (\Exception $e) {
            $this->errorCollection->setError(new \Bitrix\Main\Error($e->getMessage()));
            return null;
        }
        $entity = HighloadBlockTable::compileEntity(Config::HL_NAME);
        $addResult = $entity->getDataClass()::add($fields);
        if (!$addResult->isSuccess()) {
            $this->errorCollection->add($addResult->getErrors());
            return null;
        }
        Application::getInstance()->getTaggedCache()->clearByTag(Config::HL_NAME);
        return new Component('pvdy:feedback.add', 'success');
    }

    /**
     * @return \Bitrix\Main\Error[]
     */
    public function getErrors()
    {
        return $this->errorCollection->toArray();
    }

    /**
     * @param $code
     * @return \Bitrix\Main\Error
     */
    public function getErrorByCode($code)
    {
        return $this->errorCollection->getErrorByCode($code);
    }

    /**
     * @return array[]
     */
    public function configureActions()
    {
        return [
            'add' => [
                'prefilters' => [
                    new HttpMethod([HttpMethod::METHOD_POST]),
                    new Csrf()
                ]
            ],
        ];
    }
}