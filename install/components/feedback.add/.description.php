<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentDescription = array(
    'NAME' => GetMessage('PVDY_FEEDBACK_ADD_COMPONENT_NAME'),
    'DESCRIPTION' => GetMessage('PVDY_FEEDBACK_ADD_COMPONENT_DESCRIPTION'),
    'COMPLEX' => 'N',
    'PATH' => array(
        'ID' => 'pvdy',
        'CHILD' => array(
            'ID' => 'pvdy_feedback',
            'NAME' => GetMessage('PVDY_FEEDBACK'),
        ),
    ),
);
