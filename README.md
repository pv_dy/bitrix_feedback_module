# Форма обратной связи на Highload-блоках

### Форма:
* ФИО (обязательное)
* Е-mail (обязательное)
* Телефон
* Вопрос (обязательное)

### Условия
* Форма и список на одной странице. 
* Cписок результатов с постраничной навигацией и сортировкой по дате и ФИО. 
* Добавить обработчик события: после успешного добавления записи зарегистрировать пользователя с текущими данными 
* Верстка с Bootstrap или на свое усмотрение. 
* Отправка  формы  ajax-ом. 
* Валидация полей формы перед отправкой.

### Установка
***Требования***:   
* php = 7.4
* bitrix main >= 22.100.200

В папке /local/php_interface добавляем composer.json    
```json
{
    "name": "vendor/projectName",
    "type": "project",
    "require": {
        "wikimedia/composer-merge-plugin": "dev-master",
        "pvdy/feedback": "dev-master"
    },
    "extra": {
        "merge-plugin": {
            "require": [
                "../../bitrix/composer-bx.json"
            ]
        },
        "installer-paths": {
            "../modules/{$name}/": ["type:bitrix-d7-module"]
        }
    }
}
```
Далее
```bash
composer install
```


В папку local/modules/ добавится модуль pvdy.feedback - устанавливаем его через админку     
В папку /bitrix/ добавятся следующие компоненты:
* pvdy/feedback - комплексный
* pvdy/feedback.add
* pvdy/feedback.list

На желаемую страницу (например, /feedback/) размещаем комплексный компонент и настраиваем   
В результате в коде страницы получится примерно следующее
```php
<? $APPLICATION->IncludeComponent(
    "pvdy:feedback",
    ".default",
    array(
        "COMPONENT_TEMPLATE" => ".default",
        "ITEMS_COUNT" => "5",
        "FORM_TITLE" => "Оставить обратную связь",
        "PAGER_TEMPLATE" => "",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "SHOW_FORM" => "Y",
    ),
    false
); ?>
```