<?php

namespace Pvdy\Feedback;

use Bitrix\Main\IO\InvalidPathException;
use Bitrix\Main\IO\Path;
use Bitrix\Main\Localization\Loc;

class Config
{
    public const HL_NAME = 'PvdyFeedbackResults';
    public const HL_TABLE_NAME = 'pvdy_feedback_results';
    public const OPTION_REGISTER = 'registerAfterFeedback';

    /**
     * @return string
     * @throws InvalidPathException
     */
    public static function getLogFile(): string
    {
        return Path::normalize(__DIR__ . '/../logs/error.log');
    }

    /**
     * @return array[]
     */
    public static function getFields(): array
    {
        return [
            'UF_FIO' => [
                'FIELD_NAME' => 'UF_FIO',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                'EDIT_FORM_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_FIO')],
                'LIST_COLUMN_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_FIO')],
                'LIST_FILTER_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_FIO')],
                'ERROR_MESSAGE' => ['ru' => ''],
                'HELP_MESSAGE' => ['ru' => ''],
                'SORT' => 10,
            ],
            'UF_EMAIL' => [
                'FIELD_NAME' => 'UF_EMAIL',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                'EDIT_FORM_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_EMAIL')],
                'LIST_COLUMN_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_EMAIL')],
                'LIST_FILTER_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_EMAIL')],
                'ERROR_MESSAGE' => ['ru' => '', 'en' => ''],
                'HELP_MESSAGE' => ['ru' => '', 'en' => ''],
                'SORT' => 20,
                'SETTINGS' => [
                    'REGEXP' => "/^[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+\\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/",
                ]
            ],
            'UF_PHONE' => [
                'FIELD_NAME' => 'UF_PHONE',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'N',
                'EDIT_FORM_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_PHONE')],
                'LIST_COLUMN_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_PHONE')],
                'LIST_FILTER_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_PHONE')],
                'ERROR_MESSAGE' => ['ru' => ''],
                'HELP_MESSAGE' => ['ru' => ''],
                'SORT' => 30,
                'SETTINGS' => [
                    'REGEXP' => '/^[0-9]+$/'
                ]
            ],
            'UF_QUESTION' => [
                'FIELD_NAME' => 'UF_QUESTION',
                'USER_TYPE_ID' => 'string',
                'MANDATORY' => 'Y',
                'EDIT_FORM_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_QUESTION')],
                'LIST_COLUMN_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_QUESTION'),],
                'LIST_FILTER_LABEL' => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_QUESTION')],
                'ERROR_MESSAGE' => ['ru' => ''],
                'HELP_MESSAGE' => ['ru' => ''],
                'SETTINGS' => [
                    'ROWS' => 10
                ],
                'SORT' => 40,
            ],
            'UF_DATETIME' => [
                'FIELD_NAME' => 'UF_DATETIME',
                'USER_TYPE_ID' => 'datetime',
                'EDIT_IN_LIST' => 'N',
                'MANDATORY' => 'N',
                "EDIT_FORM_LABEL" => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_DATETIME')],
                "LIST_COLUMN_LABEL" => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_DATETIME')],
                "LIST_FILTER_LABEL" => ['ru' => Loc::getMessage('PVDY_FEEDBACK_HL_FIELD_DATETIME')],
                "ERROR_MESSAGE" => ['ru' => '', 'en' => ''],
                "HELP_MESSAGE" => ['ru' => '', 'en' => ''],
                'SETTINGS' => [
                    'DEFAULT_VALUE' => [
                        'TYPE' => 'NOW'
                    ]
                ],
                'SORT' => 150,
            ],
        ];
    }
}