<?php

namespace Pvdy\Feedback\Event;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Diag\FileLogger;
use Bitrix\Main\Diag\Helper;
use Bitrix\Main\Event;
use Bitrix\Main\IO\InvalidPathException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\Security\Random;
use Bitrix\Main\SystemException;
use Bitrix\Main\UserTable;
use Pvdy\Feedback\Config;

class FeedbackHandlers
{
    /**
     * After feedback handler. Better to use OnBeforeAdd event
     *
     * @param Event $event
     * @return void
     * @throws ArgumentException
     * @throws InvalidPathException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public static function onAfterFeedbackAdd(Event $event)
    {
        if (Option::get('pvdy.feedback', Config::OPTION_REGISTER) !== 'Y') {
            return;
        }
        $fields = $event->getParameters()['fields'];
        $requiredFields = ['UF_FIO', 'UF_EMAIL'];
        if (empty($fields)) {
            return;
        }
        foreach ($requiredFields as $fieldCode) {
            if (empty($fields[$fieldCode])) {
                return;
            }
        }
        $existUser = UserTable::query()
            ->setSelect(['ID'])
            ->where('EMAIL', $fields['UF_EMAIL'])
            ->exec()->fetch();
        if ($existUser) {
            /**
             * TODO: handle this case
             */
            return;
        }
        [$fields['LAST_NAME'], $fields['NAME'], $fields['SECOND_NAME']] = explode(' ', $fields['UF_FIO']);
        if (empty($fields['NAME']) && !empty($fields['LAST_NAME'])) {
            $fields['NAME'] = $fields['LAST_NAME'];
            $fields['LAST_NAME'] = null;
        }
        $password = Random::getString(6);
        $user = new \CUser;
        $arFields = array(
            'NAME' => $fields['NAME'],
            'LAST_NAME' => $fields['LAST_NAME'],
            'SECOND_NAME' => $fields['SECOND_NAME'],
            'EMAIL' => $fields['UF_EMAIL'],
            'LOGIN' => $fields['UF_EMAIL'],
            'PERSONAL_NOTES' => $fields['UF_QUESTION'],
            'PERSONAL_PHONE' => $fields['UF_PHONE'],
            'ACTIVE' => "Y",
            'PASSWORD' => $password,
            'CONFIRM_PASSWORD' => $password,
            'PASSWORD_EXPIRED' => 'Y',
        );

        if ((int)$user->Add($arFields) <= 0) {
            $logger = new FileLogger(Config::getLogFile());
            $logger->error($user->LAST_ERROR . "\n{date}\{trace}{delimiter}\n", [
                'trace' => Helper::getBackTrace(6, DEBUG_BACKTRACE_IGNORE_ARGS, 3)
            ]);
        }
    }
}