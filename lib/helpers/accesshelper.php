<?php

namespace Pvdy\Feedback\Helpers;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\SystemException;

class AccessHelper
{
    protected static $needleModules = ['highloadblock'];
    protected static $needleMainVersion = '22.100.200';

    /**
     * @return void
     * @throws SystemException
     * @throws LoaderException
     */
    public static function checkModules()
    {
        foreach (self::$needleModules as $module) {
            if (!Loader::includeModule($module)) {
                throw new SystemException(Loc::getMessage('PVDY_FEEDBACK_CHECK_MODULES_ERROR', ['#MODULE_NAME#' => $module]));
            }
        }
    }

    public static function checkMainVersion()
    {
        if (version_compare(SM_VERSION, self::$needleMainVersion) < 0) {
            throw new SystemException(Loc::getMessage('PVDY_FEEDBACK_CHECK_MAIN_VERSION_ERROR', ['#MAIN_VERSION#' => self::$needleMainVersion]));
        }
    }
}